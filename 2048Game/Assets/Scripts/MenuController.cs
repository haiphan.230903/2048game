using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    [SerializeField] Animator whiteImageAnim;

    public void StartButton()
    {
        whiteImageAnim.Play("end");
        Debug.Log("Start");
        StartCoroutine(Cor_LoadScene(1));
    }
    IEnumerator Cor_LoadScene(float sec)
    {
        yield return new WaitForSeconds(sec);
        SceneManager.LoadScene(1);
    }
}
