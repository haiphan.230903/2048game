using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour
{
    [SerializeField] GameObject[] objectList;
    GameObject currentObject;
    [SerializeField] float yPos, minXPos, maxXPos;
    Vector3 mousePos;
    bool isHolding = false;

    [SerializeField] float rotationSpeed = 100f;

    void Start()
    {
        SetObject();
    }
    // Update is called once per frame
    void Update()
    {
        if (GameController.Instance.isEndGame == false)
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0;
            if (currentObject != null)
            {
                if (mousePos.x < minXPos) mousePos.x = minXPos;
                if (mousePos.x > maxXPos) mousePos.x = maxXPos;
                currentObject.transform.position = new Vector2(mousePos.x, yPos);
            }

            if (Input.GetMouseButtonUp(0))
            {
                DropObject();
            }
            if (Input.GetButton("Rotate"))
            {
                RotateObject();
            }
        }
    }

    void SetObject()
    {
        isHolding = true;
        int index = Random.Range(0, 100);
        currentObject = Instantiate(objectList[index%objectList.Length], new Vector2(mousePos.x, yPos), Quaternion.EulerAngles(0, 0, Random.Range(0, 360)));
    }
    void DropObject()
    {
        isHolding = false;
        if (currentObject != null)
        {
            GameObject newObject = currentObject;
            newObject.GetComponent<Object>().Fall();
            currentObject = null;
            StartCoroutine(Cor_SetObject(1));
        }
    }
    void RotateObject()
    {
        currentObject.transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);
    }
    IEnumerator Cor_SetObject(float sec)
    {
        yield return new WaitForSeconds(sec);
        SetObject();
    }
}
