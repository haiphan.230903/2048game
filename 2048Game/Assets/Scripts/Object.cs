using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object : MonoBehaviour
{
    public GameObject NextObject;
    public Rigidbody2D rb;
    public int ID, point;
    public bool isCanMerge = true, isLanding = false;
    bool isFirstHit = true; //Dung de phat soundeffect hitClip chi mot lan dau tien
    [SerializeField] float triggerGameOverTime = 2f;

    float defaultSize, step;

    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip appearClip, hitClip;

    private float touchingDeadZoneCounter = 0f;
    private bool isTouchingDeadZone = false;

    private void Awake()
    {
        defaultSize = transform.localScale.x;
        step = defaultSize / 15;
        transform.localScale = new Vector3(0, 0, 1);
    }
    private void Start()
    {
        StartCoroutine(Cor_InitAnim(0.02f));
        audioSource.clip = appearClip;
        audioSource.Play();
    }
    private void Update()
    {
        if (isTouchingDeadZone)
        {
            touchingDeadZoneCounter+= Time.deltaTime; 
        }
    }
    IEnumerator Cor_InitAnim(float sec)
    {
        yield return new WaitForSeconds(sec);
        transform.localScale = new Vector3(transform.localScale.x + step, transform.localScale.y + step, 1);
        if (transform.localScale.x < defaultSize)
        {
            StartCoroutine(Cor_InitAnim(sec));
        }
    }
    public void Fall()
    {
        rb.velocity = Vector2.zero;
    }

    void Fusion(Vector2 fusionPos)
    {
        GameObject obj = Instantiate(NextObject, fusionPos, Quaternion.Euler(0, 0, Random.Range(0, 360)));
        GameController.Instance.IncreasePoint(point);
        obj.GetComponent<Object>().Fall();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isFirstHit == true)
        {
            audioSource.clip = hitClip;
            audioSource.Play();
            Invoke("SetIsFirstHit", 1);
        }
        if (collision.gameObject.CompareTag("Object"))
        {
            isLanding = true;
            Object collisionScr = collision.gameObject.GetComponent<Object>();
            if (collisionScr.ID == this.ID && isCanMerge)
            {
                isCanMerge = false;
                collisionScr.isCanMerge = false;
                Fusion((this.transform.position + collision.transform.position) / 2);
                Destroy(this.gameObject);
                Destroy(collision.gameObject);
            }
        }
    }

    void SetIsFirstHit()
    {
        isFirstHit = false;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("DeadZone"))
        {
            isTouchingDeadZone = true; 
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("DeadZone") && isLanding == true && Vector2.SqrMagnitude(rb.velocity) < 0.05f
            && touchingDeadZoneCounter >= triggerGameOverTime)
        {
            GameController.Instance.EndGame();
        }    
        if (collision.CompareTag("DangerZone") && isLanding == true)
        {
            GameController.Instance.isDanger = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("DangerZone") && isLanding == true)
        {
            GameController.Instance.isDanger = false;
            isTouchingDeadZone = false;
            touchingDeadZoneCounter = 0;
        }
    }
}
