using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BG : MonoBehaviour
{
    [SerializeField] AudioSource audioSource;

    public void PlayDangerMusic()
    {
        if (GameController.Instance.isEndGame == false)
        {
            audioSource.Play();
        }
    }
}
