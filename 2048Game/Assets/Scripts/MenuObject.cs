using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuObject : MonoBehaviour
{
    Animator anim;
    [SerializeField] float speed;
    [SerializeField] AudioSource audioSource;
    private void Awake()
    {
        anim = GetComponent<Animator>();
        StartCoroutine(Cor_PlayAnim(Random.Range(0f, 3.0f)));
    }

    private void Update()
    {
        transform.Rotate(Vector3.forward * speed * Time.deltaTime);
    }

    IEnumerator Cor_PlayAnim(float sec)
    {
        yield return new WaitForSeconds(sec);
        anim.Play("menuObject");
        StartCoroutine(Cor_PlayAnim(Random.Range(0.5f, 3.0f)));
    }

    public void ReverseSpeed()
    {
        speed = -speed;
    }

    private void OnMouseDown()
    {
        anim.Play("menuObject");
        audioSource.Play();
        ReverseSpeed();
    }
}
