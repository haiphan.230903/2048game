﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; private set; }

    [SerializeField] TMP_Text pointText, luckyNumberText;
    public int point = 0;

    public bool isEndGame = false, isDanger = false;
    [SerializeField] Animator bgAnim, cameraAnim, whiteImageAnim;
    [SerializeField] GameObject gameoverObj, luckyNumberObj, ButtonContainObj;

    [SerializeField] AudioSource audioSource; // Chi chua moi clip die
    private void Awake()
    {
        Instance = this;
    }
    private void Update()
    {
        bgAnim.SetBool("isDanger", isDanger);
    }
    public void EndGame()
    {
        if (isEndGame == false)
        {
            Debug.Log("ENDGAME");
            audioSource.Play();
            isEndGame = true;
            cameraAnim.Play("shake");
            Invoke("DoEndGame", 2);          
        }
    }

    void DoEndGame()
    {
        gameoverObj.SetActive(true);
        luckyNumberObj.SetActive(true);
        ButtonContainObj.SetActive(true);

        //luckyNumberText.text = Random.Range(0, 1000).ToString();
        //while (luckyNumberText.text.Length<3)
        //{
        //    luckyNumberText.text = "0" + luckyNumberText.text;
        //}
        luckyNumberText.text = point.ToString();
    }

    public void IncreasePoint(int point)
    {
        this.point += point;
        pointText.text = "Điểm: " + this.point.ToString();
    }

    public void Restart()
    {
        Debug.Log("Restart");
        whiteImageAnim.Play("end");
        StartCoroutine(Cor_LoadScene(1));
    }

    public void Back()
    {
        Debug.Log("Back");
        whiteImageAnim.Play("end");
        StartCoroutine(Cor_LoadScene(0));
    }
    IEnumerator Cor_LoadScene(int sceneIndex)
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(sceneIndex);
    }
}
